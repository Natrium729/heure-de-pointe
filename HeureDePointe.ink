# title: Heure de pointe
# author: Nathanaël Marion

-> debut

=== debut === // 29 mots.

7 heures du matin. Vous regardez défiler la mégapole derrière la vitre du train bondé. Vos yeux se ferment.

	*	Vous vous laissez somnoler.
		-> somnoler
	*	Vous vous efforcez de rester éveillé.
		-> rester_eveille

=== somnoler === // 70 mots.

Les lumières de la ville disparaissent et d’étranges rêves vous gagnent. Des pensées étrangères s’infiltrent en vous. Votre subconscient essaye-t-il de vous faire passer un message ? Un homme à l’énorme main robotisée apparaît et se jette sur vous ; vous l’étranglez en retour.

Instinctivement, sauvagement, ignorant les blessures que vous inflige votre adversaire.

Vous vous réveillez en sursaut. Les haut-parleurs diffusent la fin d’une annonce :

-> message

=== rester_eveille === // 115 mots.

Il ne fait pas encore jour, mais la ville est illuminée de panneaux publicitaires et d’écrans en tous genres, tout à fait discernables même à travers l’épais nuage de pollution et la fenêtre crasseuse.

Votre regard est attiré par l’homme à votre gauche, reflété par la vitre. Il est assez effrayant avec la scie circulaire tatouée sur son crâne chauve et son bras bionique trop gros pour sa carrure. Vous le fixez, sourcils froncés, comme s’il vous rappelait quelqu’un ; vous êtes pourtant certain de ne jamais l’avoir rencontré.

Il lève les yeux vers vous. Vous détournez prestement les vôtres. À ce moment, vous remarquez l'annonce diffusée par les haut-parleurs :

-> message

=== message === // 75 mots.

« … alors n’oubliez pas : seul l’implant 35Pr1T officiel, vendu à 798 crédits, peut vous protéger d’une attaque ! »

Comme si vous en aviez les moyens. Vous retournez à vos pensées.

Les minutes s'égrènent et les arrêts se succèdent. Enfin, l’homme à côté de vous arrive à destination. Il vous pousse sans ménagement.

L’envie de le frapper vous vient, incongrue.

	*	Vous [la chassez]essayez de la chasser.
		-> resister
	*	Vous y cédez[.] et envoyez votre poing.
		-> ceder

=== resister === // 70 mots.

Après un énorme effort de volonté, elle se dissipe et l’homme sort. Vous êtes soulagé, mais effrayé par ce qui vient de se passer.

Soudain, une vague de colère déferle dans votre esprit. Un mal de tête vous assaille et vous vous sentez comme aspiré de l’intérieur. Plié en deux, vous n’avez que le temps d’entendre l’annonce des haut-parleurs avant de devenir une coquille vide :

-> fin

=== ceder === // 83 mots.

L’homme pare le coup avec son bras de métal. Les gens autour de vous tentent de s’éloigner en criant. Vous vous jetez en avant et l’étranglez, pris d’une rage féroce. Des lames sortent du bras artificiel de votre adversaire et vous transpercent. Malgré la douleur, vous ne lâchez pas prise. Il meurt.

Vous vous détendez dans une mare de sang. Votre sang. Sourire aux lèvres, vous éprouvez un étrange sentiment de satisfaction.

Une annonce est diffusée dans les haut-parleurs :

-> fin

=== fin === // 52 mots.

« Attention, les cas de hacks de conscience se font de plus en plus fréquents ! Les victimes courent le risque de se faire voler leurs souvenirs et leurs connaissances et d’agir sous le contrôle d’un individu malintentionné ! Alors n’oubliez pas : seul l’implant 35Pr1T off… »

Puis c’est le noir.

-> END